在C#的List集合类的操作过程中，有时候我们会使用到List集合的SingleOrDefault方法和FirstOrDefault等方法，这2个方法都是System.Linq.Enumerable类为我们提供了Linq方法。那的SingleOrDefault方法和FirstOrDefault等方法有啥不同之处呢。其实除了上面2个方法，还有First()方法和Single()方法等类似方法。

以下是各个方法的不同之处，以及具体含义：
+ First方法：返回List集合序列中的第一个符合条件的元素，如果没有查找到，则抛出运行时异常。
+ FirstOrDefault方法：返回List集合序列中的第一个符合条件的元素，如果没有查找到，则返回对应默认值，如引用类型对象的话则返回null。
+ Single方法：返回List集合序列中唯一记录，如果没有或返回多条记录，则引发异常。
+ SingleOrDefault方法：返回List集合序列中唯一记录，如果该序列为空，则返回默认值；如果该序列包含多个元素，则引发异常。

### where
在C#的List集合对象中，FirstOrDefault方法可以用于查找List集合中符合条件的第一个元素，如果需要根据条件查找到List集合中的所有符合条件的元素对象集合，则需要使用到List集合的扩展方法Where()方法，Where方法的书写方式为Lambda表达式的书写形式，通过Where方法查找出符合条件的元素后再通过ToList方法可转换回原来的List集合对象类型。

### C# Database.SetInitializer 几种常见用法
```
//数据库不存在时重新创建数据库
Database.SetInitializer(new CreateDatabaseIfNotExists<SqlDbContext>());
//每次启动应用程序时创建数据库
Database.SetInitializer(new DropCreateDatabaseAlways<SqlDbContext>());
//模型更改时重新创建数据库
Database.SetInitializer(new DropCreateDatabaseIfModelChanges<SqlDbContext>());
//从不创建数据库
Database.SetInitializer<SqlDbContext>(null);
```